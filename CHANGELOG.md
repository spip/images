# Changelog

## 5.0.1 - 2025-12-02

### Added


- !4735 Implémenter les variantes `convert` et `imagick` de `image_rotation`

## 5.0.0 - 2025-11-27

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- #4722 check existance de `exif_read_data()`
- spip/spip#5974 Éviter des warnings sur `image_oriente_selon_exif()` en absence d’image

### Removed

- #4723 Filtre `|image_typo`, installer le plugin `Image typographique`
- #4723 Function `rtl_mb_ord()`, installer le plugin `Image typographique`
- #4723 Function `rtl_reverse()`, installer le plugin `Image typographique`
- #4723 Function `rtl_visuel()`, installer le plugin `Image typographique`
- #4723 Function `printWordWrapped()`, installer le plugin `Image typographique`
- #4723 Function `produire_image_typo()`, installer le plugin `Image typographique`
