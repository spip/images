<?php

declare(strict_types=1);

namespace Spip\Test\Squelettes\Balise;

use Spip\Test\SquelettesTestCase;

class ImageRecadreTest extends SquelettesTestCase
{
	public function testBonneTaille(): void {
		$skel = (fn($message_erreur) => <<<SPIP
				[(#CHEMIN{tests/data/imagetest.jpg}
					|image_passe_partout{54,54}
					|image_recadre{50,50,center}
					|image_aplatir{jpg,6C6C6C}
					|extraire_attribut{width}
					|=={50}
					|?{OK,"NOK: $message_erreur"})]
			SPIP);
		$this->assertOKCode($skel('Taille extraite incorrecte'));
		$this->assertOKCode($skel('Taille extraite incorrecte (seconde fois)'));
	}
}
