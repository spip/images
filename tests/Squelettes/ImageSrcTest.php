<?php

declare(strict_types=1);

namespace Spip\Test\Squelettes\Balise;

use Spip\Test\SquelettesTestCase;
use Spip\Test\Templating;

class ImageSrcTest extends SquelettesTestCase
{
	public function testImageSrc(): void {
		$skel = <<<SPIP
		[(#CHEMIN{tests/data/imagetest.jpg}
			|image_masque{tests/data/masque-305x85.png}
			|image_renforcement{0.1}
			|image_aplatir{jpg}
			|extraire_attribut{src})]
		SPIP;
		$tpl = Templating::fromString();
		$src = $tpl->render($skel);

		$this->assertNotEmpty($src);
		$this->assertTrue(file_exists(supprimer_timestamp($src)));

		$double = $tpl->render($skel . "\n" . $skel);
		$this->assertEquals(
			"$src\n$src",
			$double,
			'Résultat incohérent : une même chaîne de filtre produit 2 images différentes'
		);
	}
}
