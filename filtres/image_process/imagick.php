<?php

/**
 * La fonction de base qui déclare le process et detecte si il est utilisable
 * - si non renvoie null
 * - si oui renvoie les formats qu'il sait traiter en entrée et en sortie
 * @return string[]
 */
function filtres_image_process_imagick_dist(): ?array {
	if (method_exists(\Imagick::class, 'readImage')) {
		return [
			'input' => ['gif', 'jpg', 'png', 'webp', 'avif'],
			'output' => ['gif', 'jpg', 'png', 'webp', 'avif'],
		];
	}
	return null;
}

/**
 * La fonction qui cree la vignette avec le process extérieur
 */
function filtres_image_process_imagick_vignette_dist(
	string $fichier_source,
	string $format_source,
	string $fichier_dest,
	string $format_dest,
	int $width,
	int $height
): ?string {
	// Historiquement la valeur pour imagick semble differente. Si ca n'est pas necessaire, il serait preferable de garder _IMG_QUALITE
	if (!defined('_IMG_IMAGICK_QUALITE')) {
		define('_IMG_IMAGICK_QUALITE', 75);
	}
	if (!class_exists(\Imagick::class)) {
		spip_logger('images')->error('Classe Imagick absente !');

		return null;
	}

	// chemin compatible Windows
	$dir_output = realpath(dirname($fichier_dest));
	if (!$dir_output) {
		return null;
	}
	$vignette = $dir_output . DIRECTORY_SEPARATOR . basename($fichier_dest);

	try {
		$imagick = new Imagick();
		$imagick->readImage(realpath($fichier_source));
		$imagick->resizeImage(
			$width,
			$height,
			Imagick::FILTER_LANCZOS,
			1
		); //, IMAGICK_FILTER_LANCZOS, _IMG_IMAGICK_QUALITE / 100);
		$imagick->writeImage($vignette);
	} catch (\ImagickException $e) {
		spip_logger('images')->error("echec imagick sur $vignette : " . $e->getMessage());
		return null;
	}

	// $image_dest et $vignette sont normalement equivalents, mais on teste bien les deux
	if (!@file_exists($vignette) || !@file_exists($fichier_dest)) {
		spip_logger('images')->error("echec imagick sur $vignette");

		return null;
	}

	// renvoyer le chemin relatif car c'est ce qu'attend SPIP pour la suite (en particlier action/tester)
	return $fichier_dest;
}
