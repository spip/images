<?php

/**
 * La fonction de base qui déclare le process et detecte si il est utilisable
 * - si non renvoie null
 * - si oui renvoie les formats qu'il sait traiter en entrée et en sortie
 * @return string[]
 */
function filtres_image_process_convert_dist(): ?array {
	if (proces_convert_disponible()) {
		return [
			'input' => ['gif', 'jpg', 'png', 'webp', 'avif'],
			'output' => ['gif', 'jpg', 'png', 'webp', 'avif'],
		];
	}
	return null;
}

function proces_convert_disponible() {
	if (!defined('_CONVERT_COMMAND')) {
		define('_CONVERT_COMMAND', 'convert');
	}
	if (!defined('_IMG_CONVERT_QUALITE')) {
		define('_IMG_CONVERT_QUALITE', _IMG_QUALITE);
	}
	if (function_exists('exec') && _CONVERT_COMMAND !== '') {
		return true;
	}
	return false;
}

/**
 * La fonction qui cree la vignette avec le process extérieur
 */
function filtres_image_process_convert_vignette_dist(
	string $fichier_source,
	string $format_source,
	string $fichier_dest,
	string $format_dest,
	int $width,
	int $height
): ?string {
	// Securite : mes_options.php peut preciser le chemin absolu
	if (!proces_convert_disponible()) {
		return null;
	}
	if (!defined('_RESIZE_COMMAND')) {
		define(
			'_RESIZE_COMMAND',
			_CONVERT_COMMAND . ' -quality ' . _IMG_CONVERT_QUALITE . ' -orient Undefined %src -resize %xx%y! %dest'
		);
	}

	$commande = str_replace(
		['%x', '%y', '%src', '%dest'],
		[$width, $height, escapeshellcmd($fichier_source), escapeshellcmd($fichier_dest)],
		(string) _RESIZE_COMMAND
	);
	spip_logger('images')
		->info($commande);

	exec($commande, $output, $result_code);

	if (!@file_exists($fichier_dest) || filemtime($fichier_dest) < filemtime($fichier_source)) {
		spip_logger('images')->error("echec convert sur $fichier_dest");

		return null;  // echec commande
	}

	// renvoyer le chemin relatif car c'est ce qu'attend SPIP pour la suite (en particlier action/tester)
	return $fichier_dest;
}

/**
 * Version "convert" du filtre image_rotation si la librairie convert a été selectionnée dans l'admin
 * @see image_rotation()
 *
 * @param string $im
 * @param float $angle
 * @param bool $crop
 * @return string
 */
function image_rotation__process_convert($im, $angle, $crop = false) {
	$fonction = ['image_rotation__process_convert', func_get_args()];
	$image = _image_valeurs_trans($im, "rot-$angle", 'png', $fonction, false, _SVG_SUPPORTED);
	if (!$image) {
		return '';
	}
	if (
		!proces_convert_disponible()
		|| $image['format_source'] === 'svg'
		|| (defined('_ROTATE_COMMAND') && _ROTATE_COMMAND === '')
	) {
		return image_rotation($im, $angle, $crop);
	}

	$im = $image['fichier'];
	$dest = $image['fichier_dest'];

	$creer = $image['creer'];

	if ($creer) {
		spip_timer('image_rotation__process_convert');
		if (!defined('_ROTATE_COMMAND')) {
			define('_ROTATE_COMMAND', _CONVERT_COMMAND . ' -background none %src -rotate %t %dest');
		}
		spip_logger('images')
			->debug($log = "image_rotation: $im avec convert");
		$commande = str_replace(
			['%t', '%src', '%dest'],
			[$angle, escapeshellcmd($im), escapeshellcmd($dest)],
			_ROTATE_COMMAND
		);
		spip_logger('images')
			->debug($commande);
		exec($commande);
		// si echec, fallback sur le filtre natif gd2
		if (!file_exists($dest) || filemtime($dest) < filemtime($im)) {
			spip_logger('images')->error("echec image_rotation: $im avec convert");
			return image_rotation($im, $angle, $crop);
		}
		$t = spip_timer('image_rotation__process_convert');
		spip_logger('images')
			->debug("$log en $t");
	}

	[$src_y, $src_x] = taille_image($dest);

	$image_tournee = _image_ecrire_tag($image, ['src' => $dest, 'width' => $src_x, 'height' => $src_y]);
	if ($crop) {
		$image_tournee = image_recadre($image_tournee, $image['largeur'], $image['hauteur'], 'center', 'transparent');
	}
	return $image_tournee;
}
